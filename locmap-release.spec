%define product locmap
%define debug_package %{nil}
%define KOJIREPO KOJI_REPO

Name:		%{product}-release
Version:	1.0
Release:	9%{?dist}
Summary:	CERN %{product} repository release file
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:	%{product}.repo
Source1:	%{product}-qa.repo
Source2:	%{product}-testing.repo

%description
The package contains yum configuration for the CERN %{product} repository

%prep
%setup -q  -c -T
install -pm 0644 %{SOURCE0} %{SOURCE1} %{SOURCE2} .
sed -i 's/KOJIREPO/%{KOJIREPO}/g' %{product}*.repo

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 0755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 0644 %{product}*.repo $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
%files
%defattr(-,root,root,-)
%doc
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Wed Jul 12 2023 Ben Morrice <ben.morrice@cern.ch> 1.0-9
- use EL repositories 

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-8
- simplify support for EL

* Fri Oct 21 2022 Alex Iribarren <Alex.Iribarren@cern.ch> 1.0-7
- fix support for aarch64

* Wed Nov 17 2021 Ben Morrice <ben.morrice@cern.ch> 1.0-6
- add support for CentOS Stream 9

* Tue Mar 02 2021 Ben Morrice <ben.morrice@cern.ch> 1.0-5
- add support for CentOS Stream 8

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> 1.0-4
- fix locmap-qa url

* Tue Feb 18 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-3
- define the name of the stable repo as 'locmap'

* Fri Nov 29 2019 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
