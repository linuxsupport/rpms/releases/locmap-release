GIT_NAME             = $(shell basename `git rev-parse --show-toplevel`)
GIT_VERSION          = $(shell git describe --tags | sed s/v// | cut -d "-" -f 1)
GIT_RELEASE          = $(shell git describe --tags | sed s/v// | cut -d "-" -f 2)
SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE     = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})
RELEASE             ?= $(shell rpm --eval %{rhel})
_KOJI_REPO          ?= $(shell echo "potd${RELEASE}el")

rpm:
	sed -i "s/KOJI_REPO/${_KOJI_REPO}/g" $(SPECFILE)
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)

srpm:
	sed -i "s/KOJI_REPO/${_KOJI_REPO}/g" $(SPECFILE)
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)
